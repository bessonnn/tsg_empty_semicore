import z3
from time import time
from auxiliary import *

#######################################
#Author: Nicolas Besson Niebles       #
#Date: January 2024                   #
#Universite Grenoble Alpes            #
#######################################

#Returns a z3 arithmetic expression corresponding to the cost of the tour p = [0, p1, p2, ..., pm, 0].
def cost(d, p):
    return z3.Sum([d[p[i]][p[i+1]] for i, _ in enumerate(p[:-1])])


#Adds to solver a first order quantified formula that states that c(S) = cost of the minimum tour covering all 
#locations in S
def is_minimum(solver, c, d, S):
    x = z3.Real(name="x")
    P = all_paths(S)
    for p in P:
        solver.add(c[str(S)] <= cost(d, p))
    formula = z3.ForAll([x], z3.Implies(z3.And([x <= cost(d, p) for p in P]), c[str(S)] >= x))
    solver.add(formula)

#Adds to solver a first order formula that states that c(S) = cost of the minimum cost cylcle covering all 
#locations in S, it is equivalent to is_minimum(solver, c, d, S) but is quantifier free
def is_minimum_qf(solver, c, d, S):
    global F, F_expanded
    P = all_paths(S)
    for p in P:
        solver.add(c[str(S)] <= cost(d, p))
    formula = z3.Or([c[str(S)] >= cost(d, p) for p in P])
    solver.add(formula)

class stsg_generator:
    def __init__(self, nb_players):
        self.n = nb_players
        self.distance_matrix = []
        self.cost_function = []
        #N is the set of players
        self.N = [i for i in range(1, self.n+1)]
        #L is the set of all locations (one per player + the depot)
        self.L = [0] + self.N
        #U is the set of all subsets of N
        self.U = every_subset_semicore(self.N)

    def is_TSG(self, solver, c, d):
        #N is the set of players
        N = self.N
        #L is the set of all locations (one per player + the depot)
        L = self.L
        n = self.n
        #U is the set of all subsets of N
        U = self.U    
        solver.add(d[0][0] == 0)
        #We fix an optimal cycle of N to be [0, 1, 2, ..., n, 0] to break symmetries
        solver.add(c[str(N)] == cost(d, [0] + N + [0]))
        #We fix the cost of c[N] := 1000
        #solver.add(c[str(N)] == 1000)
        #The cost of every singleton [i] is twice the distance di0
        for i in N:
            solver.add(d[0][i] == c[str([i])]/2)
        #d is a symmetric matrix
        for i in L:
            for j in L:
                solver.add(d[i][j]==d[j][i])
        #The cost of every distance dij is the cost of the triangle (0, i, j, 0)
        #to which we remove the distance d0i and dj0
        for i in N:
            for j in N:
                solver.add(d[i][j] == c[str(union([i],[j])) ] - d[0][i] - d[0][j])
        #d satisfies the triangle inequality
        for i in L:
            for j in L:
                for k in L:
                    solver.add(d[i][j] + d[j][k] >= d[i][k])
        #The cost of each subset S is the cost of the minimum cost path covering S
        for S in U:
            solver.add(c[str(S)] >= 0)
            is_minimum_qf(solver, c, d, S)

    #The sum of the marginal costs is strictly larger than the cost of the grand coalition
    #iff c has an empty semicore
    def has_empty_semicore(self, solver, c):
        N = self.N
        solver.add(c[str(N)] + 1 < z3.Sum([c[str(N)] - c[str([j for j in N if j != i])] for i in N]))


    #Constructs a cost function c:2^N -> R+ that defines a TSG with with an empty semicore    
    #Also constructs a distance matrix d that defines the cost function c
    def empty_semicore_generate(self):
        #N is the set of players
        N = self.N
        #L is the set of all locations (one per player + the depot)
        L = self.L
        n = self.n
        #U is the set of all subsets of N
        U = self.U
        c = dict()
        
        #Declaration of variables c(S) for each S subset of N
        for S in U:
            c[str(S)] = z3.Real(name=str(S))

        #Declaration of auxiliary variables: the distance matrix d
        d = [[z3.Real(name=str((i,j))) for j in L] for i in L]

        solver = z3.Solver()

        self.is_TSG(solver, c, d)
        self.has_empty_semicore(solver, c)

        result = solver.check()
        if result == z3.sat:
            model = solver.model()
            self.cost_function = self.get_cost_function(model, c, U)
            self.distance_matrix = self.get_distance_matrix(model, d)
        return result
        

    def get_cost_function(self, model, c, U):
        cost_function = dict()
        for S in U:
            cost_function[str(S)] = model.evaluate(c[str(S)], model_completion=True)
        return cost_function
    
    def get_distance_matrix(self, model, d):
        return [[float(correct_string(model.evaluate(d[i][j], model_completion=True).as_decimal(20))) for j in range(self.n + 1)] for i in range(self.n + 1)]
         

if __name__ == "__main__":
    #z3.set_option(max_args=10000000, max_lines=1000000, max_depth=10000000, max_visited=1000000)
    solver = stsg_generator(6)
    pred = time()
    print(solver.empty_semicore_generate())
    post = time()
    print("It took", post - pred, "seconds")
    c = solver.cost_function
    d = solver.distance_matrix
    for l in d:
        print(l)
    empty_semicore_test(d)
