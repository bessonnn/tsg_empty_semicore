from mip import *

#Recursive function to compute all the subsets of a set
def every_subset_rec(N, i, set_of_subsets, current_subset):
    if i == len(N):
        if current_subset:
            set_of_subsets += [current_subset]
    else:
        every_subset_rec(N, i + 1, set_of_subsets, current_subset + [N[i]])
        every_subset_rec(N, i + 1, set_of_subsets, current_subset)

def absent(N, S):
    if len(S) == len(N)-1:
        for i, v in enumerate(N):
            if v not in S:
                return i
    else:
        return -1

#Input : A set (array) N of elements
#Output : An array containing all possible subsets of the set N.
def every_subset(N):
    set_of_subsets = []
    every_subset_rec(N, 0, set_of_subsets, [])
    return set_of_subsets


class core_solver:
    #N : set of players
    #c : 2^N -> R+, cost function.
    def __init__(self, N, c):
        self.ir = None
        self.m = None
        self.N = N
        self.c = c

    #Linear program returning an element of the core of the game
    #Output : array y such that y[i] is the cost allocated to player i
    def solve(self, verbose=False):
        U = every_subset(self.N)
        N = self.N
        model = Model(solver_name=CBC)
        model.verbose = 0
        y = [model.add_var() for _ in N]
        model.objective = maximize(xsum(y[N.index(i)] for i in N))
        for S in U:
            cS = self.c(S)
            if len(S) == len(self.N):
                cN = cS
            model += xsum(y[N.index(i)] for i in S) <= cS
            if verbose:
                print(S, cS)
        model += xsum(y[N.index(i)] for i in N) == cN
        model.optimize()
        if model.num_solutions:
            return [y[N.index(i)].x for i in N]
        else:
            if verbose:
                print("The core is empty !")
            return [-1 for i in N]

    #Input : An allocation of cost to the N players
    #Output : Percentage of stability constraints not violated (100% => the allocation is an element of the core)
    def is_stable(self, allocation, verbose=False):
        U = every_subset(self.N)
        N = self.N
        M = len(U)
        nb_unstable_coalitions = 0
        for S in U:
            cS = self.c(S)
            if cS < sum(allocation[N.index(i)] for i in S):
                if verbose:
                    print("It is not stable because: ", S, "is such that", cS, "< sum(",
                          [allocation[N.index(i)] for i in S], ") =",
                          sum(allocation[N.index(i)] for i in S))
                nb_unstable_coalitions += 1
        if verbose:
            print("The allocation is", 100*(1 - nb_unstable_coalitions / M), "stable")
        return 1 - nb_unstable_coalitions / M

    def is_semicore(self, allocation, verbose=False):
        ir = self.individual_rationality()
        m = self.marginal_costs()
        if verbose:
            print("Sum of marginal costs:", sum(m))
            print("Optimal cost:", sum(allocation))
        for i, y in enumerate(allocation):
            if y > ir[i] or y < m[i]:
                if verbose:
                    print("It is not semicore because: mi =", m[i], ", yi =", y, ", iri =",  ir[i])
                return False
        return True

    #Input :
    #Output : the set of individual rationalities of each player
    def individual_rationality(self):
        players = self.N
        ir = []
        for i in players:
            ir.append(self.c([i]))
        self.ir = ir
        return ir

    #Input :
    #Output : the set of incremental contributions of each player
    def marginal_costs(self):
        players = self.N
        # Compute marginal contributions of each location :
        optimal = self.c(players)
        m = []
        for j in self.N:
            cNj = self.c([i for i in players if i != j])
            m.append(optimal - cNj)
        self.m = m
        return m
