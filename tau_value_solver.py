

class tau_value_solver:
    def __init__(self, players, c):
        self.c = c
        self.N = players
        self.ir = None
        self.m = None
        self.allocation = None

    def individual_rationality(self):
        players = self.N
        ir = []
        for i in players:
            ir.append(self.c([i]))
        self.ir = ir
        return ir

    def incremental_contribution(self):
        players = self.N
        # Compute marginal contributions of each location :
        optimal = self.c(players)
        m = []
        for j in self.N:
            m.append(optimal - self.c([i for i in players if i != j]))
        self.m = m
        return m

    def solve(self):
        allocation = []
        m = self.incremental_contribution()
        ir = self.individual_rationality()
        cN = self.c(self.N)
        sum_m = sum(m[j] for j, _ in enumerate(self.N))
        sum_ir = sum(ir[j] for j, _ in enumerate(self.N))
        for i, _ in enumerate(self.N):
            if sum_ir != sum_m:
                allocation.append(m[i] + ((cN - sum_m)/(sum_ir - sum_m)) * (ir[i] - m[i]))
            else:
                allocation.append(m[i])
        self.allocation = allocation
        return allocation


