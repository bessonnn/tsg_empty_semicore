from auxiliary import *

def readDistanceMatrixFile(filename):
    with open(filename, "r") as f:
        matrixString = f.readlines()
    return [[float(i) for i in line.split(", ")] for line in matrixString]

def writeDistanceMatrixFile(distanceMatrix, filename):
    matrixString = ""
    for line in distanceMatrix:
        lineString = ""
        for i in line[:-1]:
            lineString = lineString + str(i) + ", "
        lineString = lineString + str(line[-1]) + "\n"
        matrixString += lineString
    with open(filename, "w") as f:
        f.write(matrixString)

if __name__ == '__main__':
    #Test with asymmetric TSG
    atsg_matrix = readDistanceMatrixFile('./data/proposition_5_atsg_empty_semicore.txt')
    empty_semicore_test(atsg_matrix)
    #Test with symmetric TSG
    stsg_matrix = readDistanceMatrixFile('./data/proposition_5_stsg_empty_semicore.txt')
    empty_semicore_test(stsg_matrix)