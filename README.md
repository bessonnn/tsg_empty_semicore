# Empty semicore TSG article

# Requirements:

pip install cbcpy

pip install mip

pip install z3-solver 


# To generate distance matrix with empty semicore, by default, it generates a 7x7 matrix (6 player + 1 depot), where index 0 is the depot: 


python3 stsg_empty_semicore_generate.py


# To test the emptiness of the semicore of the games provided in proposition 5:


python3 proposition_5_proof.py
