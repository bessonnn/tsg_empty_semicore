from core_solver import *
from tsp_solver import *
from tau_value_solver import *

#######################################
#Author: Nicolas Besson Niebles       #
#Date: January 2024                   #
#Universite Grenoble Alpes            #
#######################################

#Returns True if d satisfies the triangle inequality, false otherwise
def triangle_inequality_test(d):
    n = len(d)
    for i in range(n):
        for j in range(n):
            for k in range(n):
                if d[i][j] + d[j][k] < d[i][k]:
                    return False
    return True

#Returns True if d is symmetric, False otherwise
def symmetric_test(d):
    n = len(d)
    for i in range(n):
        for j in range(n):
            if d[i][j] != d[j][i]:
                return False
    return True

#Returns True if the cooperative game derived from distance matrix d has an empty semicore
#False otherwise
def empty_semicore_test(d):
    n = len(d) - 1
    N = [i for i in range(1, n+1)]
    L = [i for i in [0] + N]
    print("Satisfies triangle inequality:", triangle_inequality_test(d))
    print("Is symmetric", symmetric_test(d))
    tsp = tsp_solver(L, d)
    tau = tau_value_solver(N, lambda x: tsp.solve_optimal(x))
    allocation = tau.solve()
    core = core_solver(N, lambda x: tsp.solve_optimal(x))
    has_non_empty_semicore = core.is_semicore(allocation)
    print("Has a non empty semicore", has_non_empty_semicore)
    return has_non_empty_semicore
    #
    #In fact, the semicore is not empty if and only if the tau value is in the semicore

def empty_core_test(d):
    n = len(d) - 1
    N = [i for i in range(1, n+1)]
    L = [i for i in [0] + N]
    print("Satisfies triangle inequality:", triangle_inequality_test(d))
    print("Is symmetric", symmetric_test(d))
    tsp = tsp_solver(L, d)
    core = core_solver(N, lambda x: tsp.solve_optimal(x))
    allocation = core.solve()
    has_non_empty_core = -1 not in allocation
    print("Has a non empty core", has_non_empty_core)  

#Recursive function to compute all the subsets of a set
def every_subset_rec(N, i, set_of_subsets, current_subset):
    if i == len(N):
        if current_subset:
            set_of_subsets += [current_subset]
    else:
        every_subset_rec(N, i + 1, set_of_subsets, current_subset + [N[i]])
        every_subset_rec(N, i + 1, set_of_subsets, current_subset)

#Input : A set (array) N of elements
#Output : An array containing all possible subsets of the set N.
def every_subset(N):
    set_of_subsets = []
    every_subset_rec(N, 0, set_of_subsets, [])
    return set_of_subsets

#Input : A set (array) N of elements
#Output : An array containing all singletons [i] and sets N \ [i] for all i in N.
def every_subset_semicore(N):
    sets = [[i] for i in N]
    for i in N:
        sets += [[j for j in N if i != j]]
    for i, vi in enumerate(N):
        for j, vj in enumerate(N[i+1:]):
            sets += [[vi, vj]]
    return sets + [N]

#Compute all possible paths starting and ending at the depot 0 and going throught every location in N exactly once.
def all_paths(N):
    paths = []
    all_paths_rec(N, [0], paths)
    return paths
#Recursive auxiliary function to compute all paths in N
def all_paths_rec(N, path, paths):
    if 0 == len(N):
        paths.append(path + [0])
        return None
    for l in N:
        tmp_N = list(N)
        tmp_N.remove(l)
        all_paths_rec(tmp_N, path + [l], paths)


#The union of the sets S and Sp
def union(S, Sp):
    result = list(S)
    for i in Sp:
        if i not in result:
            result = result + [i]
    return sorted(result)

def correct_string(s):
    result = ""
    for c in s:
        if c != '?':
            result = result + c
    return result
